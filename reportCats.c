///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    reportCats.c
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include "reportCats.h"
#include <stdio.h>
#include <string.h>

void printCat(size_t index){
   if (index <= 0 || index >= amountOfCats){
      printf("animalFarm0: Bad cat [%lu] \n", index);
   }
   else {
      printf("cat index = [%lu] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] \n", index, nameList[index], genderList[index], breedList[index], isFixedList[index], weightList[index]);
   }
}

void printAllCats(){
   for (size_t i = 0; i < amountOfCats; i++){
      printf("cat index = [%lu] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] \n", i, nameList[i], genderList[i], breedList[i], isFixedList[i], weightList[i]);
   }
}
int findCat(char name[]){
   for (size_t i = 0; i < amountOfCats; i++){
      if (strcmp(name, nameList[i]) == 0){
         return i;
      }
   }
   return -1;
}
