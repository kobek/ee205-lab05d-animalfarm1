///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    utility.c
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//

#include "utility.h"
#include "catDatabase.h"
#include <string.h>
#include <stdbool.h>

bool sameNameInArray(char name[MAX_NAME_LENGTH]){
   for(size_t i = 0; i < amountOfCats; i++){
      if (strcmp(name, nameList[i]) == 0){
         return true;
      }
   }
   return false;
}

bool validIndex(size_t index){
   if (index <= 0 || index >= amountOfCats){
      return false;
   }
   return true;
}
